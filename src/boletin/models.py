from django.db import models


class Registrado(models.Model):
    nombre = models.CharField(max_length=100, blank=True)
    email = models.EmailField()
    fecha = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre
